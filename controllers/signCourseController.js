const jwt = require('jsonwebtoken');
const { transport, email } = require('../mail');
const Course = require('../models/courseModel');
const Student = require('../models/studentModel');

exports.signCourse = async (req, res) => {
    try {
        //Recive the name of the course and the id of the student
        const student = await Student.findById(req.body.id);
        const course = await Course.findOne({
            name: req.body.course
        });

        // const studentCourses = student.courses.map(async el => {
        //     let isIn = 0;
        //     const courseElement = await Course.findById(el);
        //     if (courseElement.schedule.includes(course.schedule)){
        //         isIn = 1;
        //     } else {
        //         isIn = 0;
        //     }
        //     return isIn;
        // });

        if (student.courses.length > 7) {
            res.status(400).json({
                status: 'fail',
                message: 'You have reached the maxium amount of courses (8)'
            })
        // } 
        // else if (studentCourses) {
        //     res.status(400).json({
        //         status: 'fail',
        //         message: 'You have mixed schedules, choose other course'
        //     })
        } else {
            console.log('Send email to the Teacher of the course');
            // const mailRes = await transport.sendMail({
            //         from: 'test@gmail.com',
            //         to: course.teacher.email,
            //         subject: 'A new student',
            //         html: email(
            //             `${student.name}`,
            //             `This student has been registered in you course`),
            //     },
            //     (err, info) => {
            //         if(err)
            //             console.log(err)
                    // else
                        // console.log(info);
            //     }
            // );
            const studentUptdate = await Student.findByIdAndUpdate(student._id, {
                $push: {courses: course._id},
            }, {
                new: true,
                runValidators: true
            });
            res.status(200).json({
                status: "success",
                data: {
                    student: studentUptdate
                }
            });
        }

    } catch (err) {
        res.status(400).json({
            status: 'fail',
            message: err
        })
    }
};