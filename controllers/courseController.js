const Course = require('../models/courseModel');
const Teacher = require('../models/teacherModel');

// route '/'
exports.getAllCourses = async (req, res) => {
    try {
        const courses = await Course.find();

        res.status(200).json({
            status: "success",
            results: courses.length,
            data: {
                courses
            }
        });

    } catch(err) {
        res.status(404).json({
            status: 'Fail',
            message: err
        });
    }
};

exports.createCourse = async (req, res) => {
    try {
        const newCourse = await Course.create(req.body);

        const teacherCourses = await Teacher.findById(req.body.teacher);

        if (teacherCourses.courses.length > 5) {
            res.status(400).json({
                status: 'fail',
                message: 'You have to many courses (6)'
            })
        } else {
            await Teacher.findByIdAndUpdate(
                req.body.teacher, 
                { 
                    $push: {courses: newCourse._id}
                },
                {
                    new: true,
                    runValidators: true
                }
            );
        
            res.status(200).json({
                status: "success",
                data: {
                    course: newCourse
                }
            });
        }

    } catch (err) {
        res.status(400).json({
            status: 'fail',
            message: err
        })
    }
};

// route '/:name'
exports.getOneCourse = async (req, res) => {
    try {
        const course = await Course.findOne({
            name: req.params.name
        })

        res.status(200).json({
            status: "success",
            data: {
                course
            }
        });
    } catch (err) {
        res.status(404).json({
            status: 'Fail',
            message: err
        })
    }
};

exports.updateCourse = async (req, res) => {
    try {
        const course = await Course.findOneAndUpdate(req.params.name, req.body, {
            new: true,
            runValidators: true
        });

        res.status(200).json({
            status: "success",
            data: {
                course
            }
        });
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            message: err
        })
    }
};

exports.deleteCourse = async (req, res) => {
    try {
        // TODO
        
        const course = await Course.findOne({
            name: req.params.name
        });
        
        await Teacher.updateOne({ _id : course.teacher._id}, {
            "$pull": { "courses": course._id}
        });
        
        await Course.findByIdAndDelete(course._id);

        res.status(204).json({
            status: "success",
            data: null
        });
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            message: err
        })
    }
}