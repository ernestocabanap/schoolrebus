const mongoose = require('mongoose');

const studentSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Must have a name'],
        lowercase: true
    },
    age: Number,
    dni: {
        type: Number,
        required: [true, 'Must have a identification'],
        unique: true
    },
    email: {
        type: String,
        required: [true, 'Must have a email'],
        lowercase: true,
        unique: true
    },
    courses: [
        {
            type: mongoose.Schema.ObjectId,
            ref: 'Course'
        }
    ],
    expiration: Date
});

const Student = mongoose.model('Student', studentSchema);

module.exports = Student;