const mongoose = require('mongoose');

const teacherSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Must have a name'],
        lowercase: true
    },
    age: Number,
    dni: {
        type: Number,
        required: [true, 'Must have a identification'],
        unique: true
    },
    antiquity: Number,
    email: {
        type: String,
        required: [true, 'Must have a email'],
        lowercase: true,
        unique: true
    },
    courses: [
        {
            type: mongoose.Schema.ObjectId,
            ref: 'Course'
        }
    ]
});

const Teacher = mongoose.model('teacher', teacherSchema);

module.exports = Teacher;