const mongoose = require('mongoose');
const Teacher = require('./teacherModel');

const courseSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Must have a name'],
        lowercase: true,
        unique: true
    },
    schedule: {
        type: String,
        required: [true, 'Must have a class start time']
    },
    teacher: {
        type: String,
        required: [true, 'Must have a teacher']
    },
    students: Array
});

// DOCUMENT MIDDLEWARE: runs before .save() and .create()
courseSchema.pre('save', async function(next) {
    try {
        const teacherPromise = await Teacher.findById(this.teacher);
        this.teacher = await teacherPromise;
    } catch (err) {
        console.log(err);
    }
    next();
});

const Course = mongoose.model('course', courseSchema);

module.exports = Course;