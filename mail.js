const nodemailer = require('nodemailer');

const transport = nodemailer.createTransport({
    // service: 'gmail',
    // host: process.env.MAIL_HOST,
    // port: process.env.MAIL_PORT,
    // auth: {
    //   user: process.env.MAIL_USER,
    //   pass: process.env.MAIL_PASS,
    // },
});

const email = (email, text) => `
    <div className="email" style="
        border: 1px solid black;
        padding: 20px;
        font-family: sans-serif;
        line-height: 2;
        font-size: 20px;
    ">
        <h2>Hola ${email}</h2>
        <p>A new student in you course</p>
        <p>${text}<p>
    </div>
`;

exports.transport = transport;
exports.email = email;
