const express = require('express');
const morgan = require('morgan');

const courseRouter = require('./routes/courseRoutes');
const teacherRouter = require('./routes/teacherRoutes');
const studentRouter = require('./routes/studentRoutes');
const signCourseRouter = require('./routes/signCourseRoutes');

const app = express();

// MIDDLEWARES
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
};

// Body parser, reading data from body into req.body
app.use(express.json({ limit: '10kb'}));

// Routes
app.use('/api/v1/courses', courseRouter);
app.use('/api/v1/teachers', teacherRouter);
app.use('/api/v1/students', studentRouter);
app.use('/api/v1/signcourse', signCourseRouter);

module.exports = app;

