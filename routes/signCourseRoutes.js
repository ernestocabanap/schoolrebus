const express = require('express'); 
const signCourseController = require('../controllers/signCourseController');

const router = express.Router();

router
    .route('/')
    .post(signCourseController.signCourse);

module.exports = router;
